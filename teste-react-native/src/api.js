import axios from 'axios'

// 'localhost' não funcionou no dispositivo virtual do android, 
//por esse motivo foi necessario subtituir localhost por 10.0.2.2
const url = 'http://10.0.2.2:3002';
const urlPosts = 'http://10.0.2.2:3001';

export function getAllProducts() {
    return axios.get(`${url}/products`)
        .then(resp => {
            return resp.data;
        })
}

export function getUser() {
    return axios.get(`${url}/api/v1/user`)
        .then(resp => {
            return resp.data;
        })
}

export function getUserId(userId) {
    return axios.get(`${url}/user/${userId}`)
        .then(resp => {
            return resp.data;
        })
}

export function register(name, email, password, product) {
    return axios.post(`${url}/api/v1/user`, {
        id: '',
        name: name,
        email: email, 
        avatar: '',
        password: password,
        product_id: product
    })
        .then(data => {
            return
        })        
}

export function getPosts() {
    return axios.get(`${urlPosts}/api/v1/posts`)
        .then(resp => {
            return resp.data;
        })
}

export function getPostId(id) {
    return axios.get(`${urlPosts}/posts/${id}`)
        .then(resp => {
            return resp.data;
        })
}



