import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

function Input(props) {
    return (
        <TextInput 
            style={styles.input}
            placeholder={props.placeholder}
            autoCorrect={props.autoCorrect}
            placeholderTextColor="#BBB"
            onChangeText={props.onChangeText}
            value={props.value}
            secureTextEntry={props.secureTextEntry}
            editable={props.editable}
        />
    )
}

export default Input;

const styles = StyleSheet.create({
    input: {
        backgroundColor: '#f6f6f6',
        width: '80%',
        height: 45,
        marginTop: 15,
        color: '#666666',
        fontSize: 14,
        borderRadius: 7,
        padding: 12,
        borderColor: '#DDDDDD',
        borderWidth: 1,
    }
});