import React from 'react';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import { useNavigation, NavigationContainer } from '@react-navigation/native';


function DefaultButton(props) {
    const navigation = useNavigation();

    return (
        <View style={styles.bottomTabView}>
                <TouchableOpacity style={styles.viewIconTab} onPress={()=>navigation.navigate('Perfil')}>
                    <View style={props.page == 'Perfil' ? styles.iconTabFocused : styles.iconTab}></View>
                    <Text style={props.page == 'Perfil' ? styles.bottomTextTabFocused : styles.bottomTextTab}>Perfil</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewIconTab} onPress={()=>navigation.navigate('Publicações')}>
                    <View style={props.page == 'Publicações' ? styles.iconTabFocused : styles.iconTab}></View>
                    <Text style={props.page == 'Publicações' ? styles.bottomTextTabFocused : styles.bottomTextTab}>Publicações</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewIconTab}>
                    <View style={props.page == 'Post' ? styles.iconTabFocused : styles.iconTab}></View>
                    <Text style={props.page == 'Post' ? styles.bottomTextTabFocused : styles.bottomTextTab}>Post</Text>
                </TouchableOpacity>
        </View>
    )
}

export default DefaultButton;

const styles = StyleSheet.create({
    bottomTabView: {       
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
        height: 55,
        borderTopColor: '#e8e8e8',        
        borderTopWidth: 1,
     },
     bottomTextTab: {
        fontSize: 12
     },
     bottomTextTabFocused: {
        fontSize: 12,
        color: '#34bbe1'
     },
     viewIconTab: {
        alignItems: 'center',
        justifyContent: 'center',
     },
     iconTab: {
        height: 24,
        width: 24,
        borderRadius: 50, 
        backgroundColor:'#e8e8e8',
     },
     iconTabFocused: {
        height: 24,
        width: 24,
        borderRadius: 50, 
        backgroundColor:'#34bbe1',
     }
})