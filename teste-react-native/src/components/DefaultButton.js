import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';


function DefaultButton(props) {
    return (
        <TouchableOpacity style={[ props.bgColor ? styles.btnRegistration : styles.btnSubmit ]} onPress={props.btnFunction}>
            <Text style={styles.submitText}>{props.btnText}</Text>
        </TouchableOpacity>
)
}

export default DefaultButton;

const styles = StyleSheet.create({
    btnSubmit: {
        backgroundColor: '#34bbe1',
        width: '80%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 12,
        borderRadius: 25,
    },
    btnRegistration: {
        backgroundColor: '#666666',
        width: '80%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 12,
        borderRadius: 25,
    },
    submitText: {
        color: '#FFF',
        fontSize: 15,
        fontWeight: 'bold',
    },
});