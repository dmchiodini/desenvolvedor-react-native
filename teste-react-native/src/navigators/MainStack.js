import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../pages/LoginScreen';
import ResgitrationScreen from '../pages/RegistrationScreen';
import ProfileScreen from '../pages/ProfileScreen';
import Post from '../pages/PostScreen';
import FeedScreen from '../pages/FeedScreen';

const MainStack = createStackNavigator();

export default () => {

    return (
        <MainStack.Navigator screenOptions={{
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: '#FFFFFF',
                shadowColor: 'transparent',
                elevation: 0,
                shadowOpacity: 0
            },
            headerTitleStyle:{
                color: '#000',
                fontSize: 25,
                fontWeight: 'bold'
            },
            headerBackTitleVisible: false
        }}>            
            <MainStack.Screen name='Acessar' component={LoginScreen} options={{
                headerLeft: null
            }} />
            <MainStack.Screen name='Registrar' component={ResgitrationScreen} />    
            <MainStack.Screen name="Perfil" component={ProfileScreen} options={{
                headerStyle: {
                    backgroundColor: '#34bbe1',
                    shadowColor: 'transparent',
                    elevation: 0,
                    shadowOpacity: 0
                },
                headerTitleStyle: {
                    fontSize: 25,
                    color: '#FFF'
                },
                headerLeft: null
            }} />
            <MainStack.Screen name="Publicações" component={FeedScreen} options={{
                headerLeft: null
            }} />
            <MainStack.Screen name="Post" component={Post} options={{
                headerShown: false,
            }} />      
        </MainStack.Navigator>
    )
}