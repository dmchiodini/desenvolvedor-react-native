import React, { useState, useEffect } from 'react';
import { 
    View, 
    KeyboardAvoidingView, 
    Image, 
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import { getPostId } from '../api';
import { useNavigation, NavigationContainer, useRoute } from '@react-navigation/native';
import BottomTab from '../components/BottomTab';

function PostScreen() {
    const route = useRoute();

    const [ post, setPost ] = useState([]);
    const [ id, setId ] = useState(route.params?.id);

    useEffect(()=> {
        getPostId(id)
            .then(resp => {
                setPost(resp);
            })
    },[])

    return (
        <KeyboardAvoidingView style={styles.background}>
            <ScrollView style={styles.viewScr} >
                <TouchableOpacity>
                    <Image style={styles.boxImg} source={require('../assets/bgImg.jpg')} />
                    <View>
                        <Text style={styles.titleText}>{post.title}</Text>
                        <Text style={styles.authorText}>{post.author}</Text>
                    </View>
                    <Text style={styles.textContent}>{post.content}</Text>                    
                </TouchableOpacity>
            </ScrollView>
            <BottomTab page='Post' />
        </KeyboardAvoidingView>
    )
}

export default PostScreen;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerLogo: {
        height: '30%',
    },
    container: {
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: '#FFF',
    },   
    viewScr: {
        width: '80%',
        paddingTop: 5,
    },
    boxImg: {
        height: 200,
        marginBottom: 15,
    },
    titleText: {
        fontSize: 22,
        color: '#000',
        fontWeight: 'bold',
    },
    authorText: {
        fontSize: 17,
        color: '#000',
        fontWeight: 'bold',
        marginBottom: 10,
    },
    textContent: {
        color: '#333',
        fontSize: 17,
    }
});