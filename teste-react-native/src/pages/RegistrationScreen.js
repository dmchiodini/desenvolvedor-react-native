import React, { useState, useEffect } from 'react';
import { 
    View, 
    KeyboardAvoidingView, 
    StyleSheet,
    Picker,
} from 'react-native';
import { getAllProducts, register, getUser } from '../api';
import { useNavigation, NavigationContainer } from '@react-navigation/native';
import Input from '../components/Input';
import DefaultButton from '../components/DefaultButton';

function LoginScreen() {
    const navigation = useNavigation();   

    const [ product, setProduct ] = useState("Produto");
    const [ allProducts, setAllProducts ] = useState([]);
    const [ name, setName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ users, setUsers ] = useState([]);   

    const isValid = () => name != '' && email != '' && password != '';

    useEffect(()=>{
        getAllProducts()
            .then(resp => {
                setAllProducts(resp);
            })
            .catch(err => {
                alert("nao localizado")
            })        
    },[])

    const registerUser = () => {
        if(!isValid()) {
            return alert("Preencha todos os campos!");
        } 
        getUser()
            .then(resp => {
                setUsers(resp);             
            }) 
            .then(() => {
                const validation = users.find( user => user.email == email );    
                if(validation == undefined) {
                    register(name, email, password, product)
                        .then(() => {
                            navigation.navigate('Acessar');
                        })
                        .catch(err => {
                            alert("Não foi possível cadastrar", err.message);
                        })
                } else {
                    alert('E-mail já cadastrado!');
                }
        })
    }    

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.container} >
                <Input
                    value={name}
                    onChangeText={name => { setName(name) }}
                    placeholder='Nome'
                    autoCorrect={false}
                />
                <Input
                    value={email}
                    onChangeText={email => { setEmail(email) }}
                    placeholder='Email'
                    autoCorrect={false}
                />
                <Picker
                    selectedValue={product}
                    style={styles.picker}
                    onValueChange={(itemValue, itemIndex) => setProduct(itemValue)}
                >
                    <Picker.Item label="Product" value="" />
                    {allProducts.map((product)=>{
                        return(
                            <Picker.Item label={product.name} value={product.id} key={product.id} />
                        )
                    })}                    
                </Picker>                             
                <Input
                    value={password}
                    onChangeText={password => { setPassword(password) }}
                    placeholder='Senha'
                    autoCorrect={false}
                    secureTextEntry={true}
                />
                <DefaultButton btnText='Registrar-se' btnFunction={()=>registerUser()}/>        
            </View>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    container: {
        flex: 1, 
        alignItems: 'center',
        width: '100%',
    },
    picker: {
        height: 45, 
        width: '80%',
        fontSize: 22,
        borderColor: '#DDD',
        borderWidth: 1,
        marginTop: 15,
        color: '#666666',
        backgroundColor: '#f6f6f6',
    }
});
