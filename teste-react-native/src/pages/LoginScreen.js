import React, { useState } from 'react';
import { 
    View, 
    KeyboardAvoidingView, 
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';
import { getUser } from '../api';
import { AsyncStorage } from 'react-native';
import { useNavigation, NavigationContainer } from '@react-navigation/native';
import Input from '../components/Input';
import DefaultButton from '../components/DefaultButton';


function LoginScreen() {
    const navigation = useNavigation();

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');

    const isValid = () => email != '' && password != '';

    const login = () => {
        if(!isValid()) {
            return alert("Preencha todos os campos!");
        }
        getUser()
            .then(resp => {   
                const validationEmail = resp.find( user => user.email == email );   
                if(validationEmail.email == email && validationEmail.password == password) {  
                    return (
                        AsyncStorage.setItem('data_user', JSON.stringify(validationEmail)),
                        navigation.navigate('Publicações'),
                        setPassword('') 
                        )                  
                } else {
                    alert('Dados incorretos!');
                }
            })
            .catch(() => {
                alert('Não encontrado!');
            })
    }

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.container} >
                <Input
                    value={email}
                    onChangeText={email => { setEmail(email) }}
                    placeholder='Email'
                    autoCorrect={false}
                />
                <Input
                    value={password}
                    onChangeText={password => { setPassword(password) }}
                    placeholder='Senha'
                    autoCorrect={false}
                    secureTextEntry={true}
                />
                <DefaultButton btnText='Entrar' btnFunction={()=>login()} />
                <TouchableOpacity style={styles.forgotPass} onPress={()=>{}} >
                    <Text style={styles.forgotText}>Esqueci a senha</Text>
                </TouchableOpacity>
                <DefaultButton btnText='Cadastrar' bgColor='btnRegistration' btnFunction={()=>navigation.navigate('Registrar')} />               
            </View>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    container: {
        flex: 1, 
        alignItems: 'center',
        width: '100%',
    },
    forgotPass: {       
        marginTop: 10, 
        marginBottom: 20,
     },
     forgotText: {
         color: '#34bbe1',
         fontWeight: 'bold',
     }  
});
