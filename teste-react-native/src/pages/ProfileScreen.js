import React, { useState, useEffect } from 'react';
import { 
    View,
    Image, 
    StyleSheet,
    Text,
    KeyboardAvoidingView,
} from 'react-native';
import { getAllProducts } from '../api';
import { AsyncStorage } from 'react-native';
import { useNavigation, NavigationContainer } from '@react-navigation/native';
import BottomTab from '../components/BottomTab';
import Input from '../components/Input';
import DefaultButton from '../components/DefaultButton';

function ProfileScreen() {
    const navigation = useNavigation();

    const [ product, setProduct ] = useState("Produto");
    const [ allProducts, setAllProducts ] = useState([]);
    const [ user, setUser ] = useState({});
    const [ email, setEmail ] = useState('');    
    
    useEffect(()=> {
        AsyncStorage.getItem('data_user')
            .then((data_user) => {        
                if (data_user == null) {
                    navigation.reset('Acessar');
                } else {
                    const userData = JSON.parse(data_user);
                    setUser(userData);                      
                }       
            })
            .catch((err) => {
                console.log('erro: ', err);
            }); 

        getAllProducts()
            .then(resp => {
                setAllProducts(resp);
            })
            .catch(err => {
                alert("nao localizado");
            });      
    }, [])

    const logout = async () => {
        try {
          await AsyncStorage.removeItem('data_user');
          navigation.replace('Acessar');
        }
        catch(err) {
            return console.log(err);
        }
    }

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.containerLogo}>
                <Image 
                    style={styles.imgBox}
                    source={require('../assets/avatar.jpg')}
                />
            </View>
            <View style={styles.container} >
                <Text style={styles.profileText}>{user.name}</Text>
                {allProducts.map((product)=>{
                    if(user.product_id == product.id) {
                        return(
                            <Input
                                value={product.name}
                                editable={false}
                                key={product.id}
                            />
                        )
                    }
                })}                        
                <Input
                    value={user.email}
                    placeholder='Email'
                    autoCorrect={false}
                    editable={false}
                />                
                <DefaultButton btnText='Fazer Logout' btnFunction={()=>logout()} />
            </View>
            <BottomTab page='Perfil' />
        </KeyboardAvoidingView>
    )
}

export default ProfileScreen;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#FFFFFF',
    },
    containerLogo: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        backgroundColor: '#34bbe1',
    },
    container: {
        flex: 1, 
        alignItems: 'center',
        width: '100%',
    },   
    imgBox: {
        width: 150,
        height: 150,
        borderRadius: 100,
    },
    profileText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000000',
        margin: 10,
    }
});
