import React, { useState, useEffect } from 'react';
import { 
    View, 
    KeyboardAvoidingView, 
    Image, 
    StyleSheet,
    TextInput,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { AsyncStorage } from 'react-native';
import { useNavigation, NavigationContainer, useRoute } from '@react-navigation/native';
import { getPosts } from '../api';
import BottomTab from '../components/BottomTab';

function FeedScreen() {
    const navigation = useNavigation();

    const [ user, setUser ] = useState({});
    const [ posts, setPosts ] = useState([]);
    const [ id, setId ] = useState('');

    useEffect(()=> {
        AsyncStorage.getItem('data_user')
            .then((data_user) => {        
                if (data_user == null) {
                    navigation.reset('Acessar');
                } else {
                    const userData = JSON.parse(data_user);
                    setUser(userData)  ;                      
                }       
            })
            .catch((err) => {
                console.log('erro: ', err);
            });

        getPosts()
            .then(resp => {
                setPosts(resp);
            });
    },[])

    const handleClick = (id) => {
        setId(id);
        navigation.replace('Post', {id});
    }

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.container}>
                <TextInput 
                    style={styles.inputSearch}
                    placeholder='Search'
                />
            </View>           
            <ScrollView style={styles.viewScr}>
                {posts.map((post)=>{
                   if(post.products.find(el => el == user.product_id)) {
                        return (
                            <TouchableOpacity style={styles.containerPost} onPress={() => handleClick(post.id)} key={post.id}>
                                <Image style={styles.boxImg} source={require('../assets/bgImg.jpg')} />
                                <Text style={styles.titleText}>{post.title}</Text>
                                <TextInput 
                                    maxLength={100} 
                                    style={styles.textContent}
                                    value={post.content}
                                    editable={false}
                                    multiline={true}
                                    />
                                <Text style={styles.dataText}>{post.date}</Text>
                                <View style={styles.ballContainer}>
                                    <View style={styles.firstBall}></View>
                                    <View style={styles.ball}></View>
                                    <View style={styles.ball}></View>
                                </View>
                            </TouchableOpacity>
                        )
                   }
                    
                })}                
            </ScrollView>
            <BottomTab page='Publicações' />
        </KeyboardAvoidingView>
    )
}

export default FeedScreen;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
    },
    inputSearch: {
        backgroundColor: '#f6f6f6',
        width: '100%',
        height: 45,
        marginTop: 15,
        color: '#000000',
        fontSize: 14,
        borderRadius: 30,
        padding: 12,
        borderColor: '#DDDDDD',
        borderWidth: 1,
    },
    container: {
        alignItems: 'center',
        width: '80%',
        backgroundColor: '#FFF',
        marginBottom: 10,
    },  
    viewScr: {
        width: '80%',
    },
    containerPost:{
        marginBottom: 20,
        justifyContent: 'space-between',
    },
    boxImg: {
        width: '100%',
        height: 200,
        borderRadius: 7,
        marginBottom: 5,
    },
    titleText: {
        fontSize: 16,
        color: '#000',
        fontWeight: 'bold',
        marginBottom: -5,
    },
    textContent: {
        color: '#333',
    },
    dataText: {
        color: '#AAA',
        marginBottom: 5,
    },
    ballContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    ball: {
        height: 8,
        width: 8,
        borderRadius: 8,
        backgroundColor: '#e8e8e8',
        marginLeft: 10
    },
    firstBall: {
        height: 8,
        width: 8,
        borderRadius: 8,
        backgroundColor: '#34bbe1',
        marginLeft: 10,
    }
});